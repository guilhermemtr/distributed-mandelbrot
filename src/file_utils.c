#include "file_utils.h"

#ifdef __FILE_UTILS__

extern color_t color_map[];

/* writes a buffer to a pgm file */
void
write_pgm(char* fn, byte* buf, unumber w, unumber h, unumber max)
{
  unumber i, ix;
	FILE *file;
  file = fopen(fn, "w");
  if(file == NULL)
  {
	  printf("Error opening file %s\n", fn);
	  exit(1);
  }
  fprintf(file, "P3\n");
  fprintf(file, NUM" "NUM"\n", w, h);
  fprintf(file, NUM"\n",max);
  for(i = 0; i < w * h; i++)
  {
    if(!(i % w))
    {
      fprintf(file, "\n");
    }
    ix = 3 * (number)buf[i];
    fprintf(file, NUM" "NUM" "NUM" ", color_map[ix], color_map[ix+1], color_map[ix+2]);
  }
  fclose(file);
}

/* writes the histogram to a stream */
void
write_hist(FILE* f, unumber* hist)
{
  fprintf(f, "Histogram values:\n");
  unumber i;
  for(i = 0; i < NUM_COLOR_VALUES; i++)
  {
    fprintf(f, "color ("UNUM"):\t"UNUM"\n", i, hist[i]);
  }
}

#endif /* __FILE_UTILS__ */